/**
 *  JavaScript Format Framework
 *  Use the W3C 'revealing module pattern' format.
 *  @link	{https://www.w3.org/community/webed/wiki/JavaScript_best_practices}
 *
 *  @namespace
 */
var frameWork = function() {

  let companyMeta = new Vue({
    el: '#company-meta',
    data: {
      items: [
        { id: "company", name: "Company:", editable: true, type: "text" },
        { id: "type", name: "Type:", editable: true, type: "text"},
        { id: "delivery", name: "Delivery:", editable: true, type: "text"},
        { id: "terms", name: "Terms:", editable: true, type: "text"},
        { id: "expiration", name: "Expiration date:", editable: false, type: "date"},
        { id: "initial", name: "Initial cost:", editable: false, type: "number"},
        { id: "monthly", name: "Monthly cost:", editable: false, type: "text"},
        { id: "record", name:"Record:", editable: false, type: "number", hidden: true}
      ],
      json:{
        company: "NA",
        type: "NA",
        delivery: "NA",
        terms: "NA",
        expiration: "NA",
        initial: "NA",
        monthly: "NA",
        record: "NA"
      }
    }
  });



  let initalCostsList = new Vue({
    el: "#initial-costs",
    data:{
      initial: 0,
      json:  [
      ]
    },
    methods:{
      onUpdate: function(){
        for(let i in initalCostsList.json){
          initalCostsList.json[i].item = Number(i) + 1;
        }
      }
    }
  })


  /**
	 * Format labels and inputs for the header details,
	 *  includes company name.
	 *
	 * @data - The JSON data.
	 */
	function company(data) {
    //console.log("company:", data);
    companyMeta.json.company = data.company;
    companyMeta.json.delivery = data.delivery;
    companyMeta.json.record = data.record;
    companyMeta.json.terms = data.terms;
    companyMeta.json.type = data.type;
  }


  /**
	 * Format table and inputs for the install details.
	 *
	 * @data - The JSON data.
	 */
	function install(data) {
		// Add Consumption Tax to data.
		// This is NOT the desired behaviour.
		// Used this way, ONLY to show the proper information.
		let installData = addTax(data);

    console.log(installData);

		target = 'install';

    initalCostsList.json = installData;

		// Populate Totals
		let total = costs(installData);
		companyMeta.json.initial = total;
    initalCostsList.initial = total;

		//console.log(installData);
	}


  /**
   * Calculates the totals for the install and monthly
   *  sections.
   *
   * @data - The JSON data.
   *
   */
  function costs(data) {

    let total = 0;
    $.each(data , function( key, value ) {
      total += value.amount;
    });

    return total;

  }

  /**
	 * Add consumption tax as an array item.  This is
	 *  NOT the desired usage.  But it's the easiest
	 *  way to show the data for the prototype.
	 *
	 * @data - The JSON data.
	 */
	function addTax(data) {
		total = 0;

		$.each(data , function( key, value ) {
			total += value.amount;
		});

		// 8% - Round Down
		tax = Math.floor(total * 0.08);

		data.push({
			'record': '',
			'item': data.length + 1,
			'desc': 'Consumption Tax',
			'qty': 1,
			'unit': '8%',
			'price': tax,
			'amount': tax,
			'comment': ''

		});

		return data;

	}


  /**
	 * Get the JSON data from server.
	 *
	 * @url - The URL of the server.
	 * @callback - The related function to use.
	 */
	function loadJSON(url, callback) {
		$.getJSON( url , function() {
		})
		.done(function(data) {
			callback(data);
			// do.something(element, data);
			//console.log(data);
			//$.each(data , function( key, value ) {
				// do.something(element, data);
			//});
		});
	}



  /**
	 * Calculates the expire date.  Default: 4 weeks from today.
	 *  Just shown on the page for reference.
	 *
	 * @weeks - Number of weeks
	 */
	function expireDate(weeks = 4) {
		dt = new Date();
		nd = add_weeks(dt, weeks);

		companyMeta.json.expiration = nd.toISOString().split("T")[0];
	}

	/**
	 * Add weeks, used by expireDate().
	 *
	 * @dt = Date
	 * @n = Number of weeks to add.
	 */
	function add_weeks(dt, n)
	 {
		 return new Date(dt.setDate(dt.getDate() + (n * 7)));
	 }


  /**
	 * Initialization - Functions to call on start.
	 */
	function init() {
		// Run against random formatted JSON data
		loadJSON('http://json.invite-comm.jp/api/json/company', company);
		loadJSON('http://json.invite-comm.jp/api/json/install', install);
		//loadJSON('http://json.invite-comm.jp/api/json/monthly', monthly);
		//loadJSON('http://json.invite-comm.jp/api/json/notes', notes);
		expireDate();
	}

	/**
	 * Return - Expose functions to external calls.
	 *  Only init() is exposed.
	 */
	return {
		init:init,
	}



}();


jQuery(document).ready(function() {
	frameWork.init();
});
