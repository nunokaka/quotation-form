/**
 *  JavaScript Format Framework
 *  Use the W3C 'revealing module pattern' format.
 *  @link	{https://www.w3.org/community/webed/wiki/JavaScript_best_practices}
 *
 *  @namespace
 */
var frameWork = function() {

	/**
	 * Static arrays available to all frameWork modules.
	 *  The data SHOULD reflect the current page.
	 */
	var companyData = '';
	var installData = '';
	var monthlyData = '';
	var notesData = '';

	/**
	 * Get the JSON data from server.
	 *
	 * @url - The URL of the server.
	 * @callback - The related function to use.
	 */
	function loadJSON(url, callback) {
		$.getJSON( url , function() {
		})
		.done(function(data) {
			callback(data);
			// do.something(element, data);
			//console.log(data);
			//$.each(data , function( key, value ) {
				// do.something(element, data);
			//});
		});
	}

	/**
	 * Format labels and inputs for the header details,
	 *  includes company name.
	 *
	 * @data - The JSON data.
	 */
	function company(data) {

		companyData = data;
		target = 'company';

		var element = document.getElementById(target);
		var form = document.createElement('form');

		$.each(data , function( key, value ) {

			var label = document.createElement('label');
			label.htmlFor = key;
			label.innerHTML = key + ':&nbsp;';

			var input = document.createElement('input');
				input.type = 'text';
				input.name = key;
				input.id = key;
				input.value = value;
				if (key == 'record') {
					input.type = 'hidden';
				}

			var div = document.createElement('div');
				div.className = 'row';

				if (key != 'record') {
					div.appendChild(label);
				}
				div.appendChild(input);

			form.appendChild(div);

		});

		element.appendChild(form);
	}

	/**
	 * Format table and inputs for the install details.
	 *
	 * @data - The JSON data.
	 */
	function install(data) {
		// Add Consumption Tax to data.
		// This is NOT the desired behaviour.
		// Used this way, ONLY to show the proper information.
		installData = addTax(data);
		target = 'install';

		var element = document.getElementById(target);
			element.appendChild(createTable(data, true));

		// Populate Totals
		costs(installData, 'install_cost');
		costs(installData, 'install_cost2');

		//console.log(installData);
	}

	/**
	 * Format table and inputs for the monthly details.
	 *
	 * @data - The JSON data.
	 */
	function monthly(data) {
		// Add Consumption Tax to data.
		// This is NOT the desired behaviour.
		// Used this way, ONLY to show the proper information.
		monthlyData = addTax(data);
		target = 'monthly';

		var element = document.getElementById(target);
			element.appendChild(createTable(data, true));

		// Populate Totals
		costs(monthlyData, 'monthly_cost');
		costs(monthlyData, 'monthly_cost2');

		//console.log(installData);
	}

	/**
	 * Format table and inputs for the notes.
	 *
	 * @data - The JSON data.
	 */
	function notes(data) {
		notesData = data;
		target = 'notes';

		var element = document.getElementById(target);
			element.appendChild(createTable(data, false));

		//console.log(installData);
	}

	/**
	 * Add consumption tax as an array item.  This is
	 *  NOT the desired usage.  But it's the easiest
	 *  way to show the data for the prototype.
	 *
	 * @data - The JSON data.
	 */
	function addTax(data) {
		total = 0;

		$.each(data , function( key, value ) {
			total += value.amount;
		});

		// 8% - Round Down
		tax = Math.floor(total * 0.08);

		data.push({
			'record': '',
			'item': data.length + 1,
			'desc': 'Consumption Tax',
			'qty': 1,
			'unit': '8%',
			'price': tax,
			'amount': tax,
			'comment': ''

		});

		return data;

	}

	/**
	 * Create a generic table to show the prototype
	 *  information.
	 *
	 * @data - The JSON data.
	 * @header bool - Create the <th> tags.  Not used for notes.
	 */
	function createTable(data, header) {

		var table = document.createElement('table');
			table.style.border = 'thin dotted red';

		if (header) {
			table.appendChild(tableHeader(data));
		}

		$.each(data , function( key, value ) {

			var tableRow = document.createElement('tr');

			$.each(value , function( row, col ) {
				if (row != 'record') {
					var tableData = document.createElement('td');
						tableData.style.border = 'thin dotted red';

						var input = document.createElement('input');
							input.value = col;
							input.id = value.record;
							input.name = row;
							tableData.appendChild(input);

					tableRow.appendChild(tableData);
				}
			});

			table.appendChild(tableRow);
		});
		return table;

	}

	/**
	 * Create the table <th> tags, based on the JSON key.
	 *  Not used for notes.
	 *
	 * @data - The JSON data.
	 */
	function tableHeader(data) {
		var tableRow = document.createElement('tr');
		$.each(data[0] , function( key, value ) {
			// do.something(element, data);
			if (key != 'record') {
				var header = document.createElement('th');
					header.innerHTML = key;
					header.style.border = 'thin dotted red';
				tableRow.appendChild(header);
			}
		});
		return tableRow;
	}

	/**
	 * Calculates the totals for the install and monthly
	 *  sections.  It updates the DOM directly.
	 *
	 * @data - The JSON data.
	 * @target DOM - Element ID to populate with value.
	 */
	function costs(data, target) {

		total = 0;
		$.each(data , function( key, value ) {
			total += value.amount;
		});

		var element = document.getElementById(target);
			element.innerHTML = total;

	}

	/**
	 * Calculates the expire date.  Default: 4 weeks from today.
	 *  Just shown on the page for reference.
	 *
	 * @weeks - Number of weeks
	 */
	function expireDate(weeks = 4) {
		dt = new Date();
		nd = add_weeks(dt, weeks);

		var element = document.getElementById('expire');
			element.innerHTML = nd.toISOString().split("T")[0];
	}

	/**
	 * Add weeks, used by expireDate().
	 *
	 * @dt = Date
	 * @n = Number of weeks to add.
	 */
	function add_weeks(dt, n)
	 {
		 return new Date(dt.setDate(dt.getDate() + (n * 7)));
	 }

	/**
	 * onClick
	 */
	 $("#headerInfo").click(function(){
	 	alert(JSON.stringify(companyData));
	 	console.log(companyData);
	 });

	 $("#installInfo").click(function(){
	 	alert(JSON.stringify(installData));
	 	console.log(installData);
	 });

	 $("#monthlyInfo").click(function(){
	 	alert(JSON.stringify(monthlyData));
	 	console.log(monthlyData);
	 });

	 $("#notesInfo").click(function(){
	 	alert(JSON.stringify(notesData));
	 	console.log(notesData);
	 });

	/**
	 * Initialization - Functions to call on start.
	 */
	function init() {
		// Run against random formatted JSON data
		loadJSON('http://json.invite-comm.jp/api/json/company', company);
		loadJSON('http://json.invite-comm.jp/api/json/install', install);
		loadJSON('http://json.invite-comm.jp/api/json/monthly', monthly);
		loadJSON('http://json.invite-comm.jp/api/json/notes', notes);
		expireDate();
	}

	/**
	 * Return - Expose functions to external calls.
	 *  Only init() is exposed.
	 */
	return {
		init:init,
	}

}();

jQuery(document).ready(function() {
	frameWork.init();
});
